/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package inscricao.faces.mngbeans;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;
import javax.faces.context.FacesContext;
/**
 *
 * @author lucas
 */

@ManagedBean(name="loginBean", eager=true)
//@RequestScoped
@ApplicationScoped
public class LoginBean {
    
    private String usuario;
    private String senha;
    private Boolean admin;
    
    public String getUsuario()
    {
        return usuario;
    }
    
    public void setUsuario(String usuario)
    {
        this.usuario = usuario;
    }
    
    public String getSenha()
    {
        return senha;
    }
    
    public void setSenha(String senha)
    {
        this.senha = senha;
    }
    
    public Boolean getAdmin()
    {
        return admin;
    }
    
    public void setAdmin(Boolean admin)
    {
        this.admin = admin;
    }
    
    public String login()
    {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        if(usuario.equals(senha))
        {
            if(admin)
            {
                return "admin";
            }
            else
                return "cadastro";
            
        }
        else
        {
            facesContext.addMessage("form", new FacesMessage("Acesso negado"));
            return null;
        }
      }
}